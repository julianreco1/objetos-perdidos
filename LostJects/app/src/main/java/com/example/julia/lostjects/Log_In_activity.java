
package com.example.julia.lostjects;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class Log_In_activity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    DatabaseReference database;


    private GoogleApiClient googleApiClient;

    private SignInButton signInButton;

    private String correo;
    private List<User> userList=new ArrayList<>();
    User user=new User();
    String TAG="prueba";

    public static final int SIGN_IN_CODE = 777;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log__in_activity);
        database=FirebaseDatabase.getInstance().getReference("User");
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        signInButton = (SignInButton) findViewById(R.id.signInButton);

        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent, SIGN_IN_CODE);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == SIGN_IN_CODE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
    }

    public void handleSignInResult(GoogleSignInResult result) {
        if (result.isSuccess()) {

            correo=result.getSignInAccount().getEmail();
            //el correo lo captura bien
            Log.i(TAG,"agg");
            goMainScreen();
        }

        else {
            Toast.makeText(this,"no es posible iniciar sesion", Toast.LENGTH_SHORT).show();
        }
    }

    public void goMainScreen() {
        /*Intent intent;
        Log.i("algo","el correo es:"+correo);
        if(correo.equals("crisrxn12@gmail.com"))
        {//adm
            intent = new Intent(this,Menuadm.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
        else
        {
            if(correo=="julianreco1@gmail.com")
            {
                //ventana usuario
                intent = new Intent(this,MenuUsu.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
            else
            {

                String clave=database.push().getKey();
                User usuario=new User(correo,2,1);
                database.child(clave).setValue(usuario);
                intent = new Intent(this,MenuUsu.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }*/
       // Log.i(TAG,"ingreso hasta aqui");
        Query q=FirebaseDatabase.getInstance().getReference().child("User").orderByChild("correo").equalTo(correo).limitToFirst(1);
        //Log.i(TAG,"ingreso hasta aqui");
        q.addListenerForSingleValueEvent(ValueEventListener);
        //Log.i(TAG,"ingreso hasta aqui");

        //Log.i(TAG,"tamaño="+userList.size());
       // Log.d("hola232","esto es+"+userList.get(0).toString());


        //dejemos estonces asi el loguin,entonces pone todo lo de las interfaces porfa, si, si te da tiempo podrias poner lo del chat del video q te mande
        //men en lo del simplemente en firebase se hace esta estructura:
        /*
        * salas
        *   mens1
        *   ....
        *   entonces para cada objeto hay una unica sala,no, es para cada objeto hay una sala miralo como un objeto, entonces
        *   se capturaria la llave del objeto perdido,el comprador y el vendedor y si los parametros son iguales se hace a un chat
        *   si me hago entender? seria como por medio de una consulta?????julian....
        *   acordate que cada objeto en la tienda tiene el correo del vendedor mira lo del entregable 3 por eso te dijo q hay que mirar los campos
        *   no se abria que mirar eso pero si me entendes la idea??.....si, y como ambos van a obtener la llave del objeto ahi se garantiza,en el video
        *   de  eventos en un recicler view muestra como capturar un dato de ese recicler view hacia otra ventana.
        *
        *   entonces faltaria modificar bien lo de objetos
        *   chat
        *   botones
        *   y pdf
        *   ah y por cierto a la hora que el men valide ya tengo el codigo para enviar un correo para que reclamen el objeto perdido..
        *   que mas faltaria??si,pero basate en el modelo de base de datos del entregable 3.
        * */

        }

    public void validar() {
        Log.i(TAG,"tam"+userList.size());



            if (userList.get(0).getTipo_user()==1)
            {
                //admin
                Intent intent = new Intent(this,Menuadm.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);//correla a ver , si
            }
            else {
                //usuario normal
                Intent intent = new Intent(this, MenuUsu.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.putExtra("propio",correo);

                startActivity(intent);
            }



        }



    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {






    }
    ValueEventListener ValueEventListener = new ValueEventListener(){

        public void onDataChange(DataSnapshot dataSnapshot)
        {
            Log.i(TAG,"ingreso hasta aqui");
            if(dataSnapshot.exists())
            {
                for (DataSnapshot snapshot:dataSnapshot.getChildren()) {
                    Log.i(TAG,"safmfksamolsafnmñn");
                    Log.i(TAG,"algo"+snapshot.toString());
                    System.out.print(snapshot.toString());
                    user=snapshot.getValue(User.class);
                    Log.i(TAG,user.toString());
                    userList.add(0,user);
                    Log.i(TAG,"tamaño="+userList.size());
                    validar();
                }
            }
            else
            {
                String clave=database.push().getKey();
                User usuario=new User(correo,2,1);
                database.child(clave).setValue(usuario);
                Intent intent = new Intent(Log_In_activity.this,MenuUsu.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };



}
