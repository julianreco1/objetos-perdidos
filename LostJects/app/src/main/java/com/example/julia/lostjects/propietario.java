package com.example.julia.lostjects;

import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.InputStream;

public class propietario extends AppCompatActivity {
    private StorageReference storeref;
    private StorageReference storageReference;
    private FirebaseDatabase database;
    private DatabaseReference databaseReference;
    private Uri uri=null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_propietario);
        Button doc = findViewById(R.id.btndoc);
        Button ev = findViewById(R.id.btnev);
        storageReference = FirebaseStorage.getInstance().getReference();
        databaseReference = database.getInstance().getReference().child("PosiblesPropietarios");
        storeref = FirebaseStorage.getInstance().getReference();
        doc.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent galleryintent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryintent.setType("application/pdf");
                startActivityForResult(galleryintent, 2);
            }


        });
        ev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent galleryintent = new Intent(Intent.ACTION_GET_CONTENT);
                galleryintent.setType("application/pdf");
                startActivityForResult(galleryintent, 2);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 2 && resultCode == RESULT_OK) {
            uri = data.getData();

        }


    }
    public void submitButtonClicked(View view){

        Uri Uri2;
            StorageReference filePath = storageReference.child("PostPdf").child(uri.getLastPathSegment());
            StorageReference filePath2 = storageReference.child("PostPdf").child(uri.getLastPathSegment());
            filePath.putFile(uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Uri downloadurl = taskSnapshot.getDownloadUrl();
                    Uri downloadur2=taskSnapshot.getDownloadUrl();
                    Toast.makeText(propietario.this,"Upload Complete", Toast.LENGTH_LONG ).show();
                    DatabaseReference newPost = databaseReference.push();
                    newPost.child("objeto").setValue("diagrama");
                    newPost.child("propietario").setValue("");
                    newPost.child("documento").setValue(downloadurl.toString());
                    newPost.child("evidencia").setValue(downloadur2.toString());
                    newPost.child("recuperado").setValue(0);

                }
            });

    }
}