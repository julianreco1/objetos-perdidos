package com.example.julia.lostjects;

import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Adapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.List;

public class TiendaNueva extends AppCompatActivity {
    private RecyclerView recyclerView;
    private TiendaAdapter adapter;
    private ArrayList<PostTienda1> objetostienda;
    private DatabaseReference dbTienda;
    private String TAG = "prueba";
    private String correo;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tienda_nueva);
        String TAG="prueba";
        Bundle objeto=getIntent().getExtras();
        if(objeto != null)
        {

            correo=objeto.getString("propio");
        }
        Log.i(TAG,"añgo1 "+correo);
        TextView alcorreo=(TextView)findViewById(R.id.txtCorreopropio);
        TextView alcorreo1=(TextView)findViewById(R.id.txtvendedor);
        alcorreo.setVisibility(View.INVISIBLE);
        alcorreo.setText(correo);

        Log.i(TAG,"añgo1 "+alcorreo.getText());
        recyclerView = (RecyclerView) findViewById(R.id.reciclerview);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //crear array list de objetos tienda llamado mydataset
        objetostienda = new ArrayList<>();
        adapter = new TiendaAdapter(objetostienda);
        recyclerView.setAdapter(adapter);


        //adapter=new TiendaAdapter(myDataset);
        //consultar objetos
        dbTienda = FirebaseDatabase.getInstance().getReference().child("TiendaNueva");
        /*String clave=dbTienda.push().getKey();
        Uri algo= null;
        PostTienda1 obj=new PostTienda1(algo,"asd","adnkaD","25225","@SHBDH","@DHSAH",clave);

        dbTienda.child(clave).setValue(obj);*/
        dbTienda.addListenerForSingleValueEvent(ValueEventListener);
    }


    ValueEventListener ValueEventListener = new ValueEventListener() {

        public void onDataChange(DataSnapshot dataSnapshot) {
            Log.i(TAG, "ingreso hasta aqui");
            //objetostienda.clear();
            if (dataSnapshot.exists()) {
                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {

                    Log.i(TAG,snapshot.toString());
                    PostTienda1 obj_tienda=new PostTienda1();
                    obj_tienda= snapshot.getValue(PostTienda1.class);
                    obj_tienda.setComprador(correo);
                    objetostienda.add(obj_tienda);
                }
                adapter.notifyDataSetChanged();

                Log.i(TAG, "ingreso hasta aqui, tamaño"+objetostienda.size());

            }

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };
}
