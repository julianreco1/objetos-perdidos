package com.example.julia.lostjects;

public class User {
    private String correo;
    private int tipo_user;
    private int status;

    @Override
    public String toString() {
        return
                "correo='" + correo + '\'' +
                ", tipo_user=" + tipo_user +
                ", status=" + status +
                '}';
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setTipo_user(int tipo_user) {
        this.tipo_user = tipo_user;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public  User(String correo, int tipo, int status)
    {
        this.correo=correo;
        this.tipo_user=tipo;
        this.status=status;
    }

    public  User()
    {
        this.correo="";
        this.tipo_user=2;
        this.status=1;
    }

    public String getCorreo()
    {
        return correo;
    }

    public int getStatus() {
        return status;
    }

    public int getTipo_user() {
        return tipo_user;
    }
}


