package com.example.julia.lostjects;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class Chat extends AppCompatActivity {
    private Query consulta,c1,c2;
    private String TAG="prueba";
    private String key;
    private GoogleSignInResult newGoogleSignInResult;
    String correo= "@algo";
    String vendedor;
    Msm mens;
    RecyclerView recyclerView;
    ChatAdapter adapter;
    ArrayList<Msm> msj;
    Msm mensaje=new Msm();
    Sala sala;
    String salita;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        final Bundle objeto=getIntent().getExtras();

        if(objeto != null)
        {
           key=objeto.getString("objeto");
           correo=objeto.getString("propio");
            vendedor=objeto.getString("vendedor");
        }
        final TextView mens=(TextView)findViewById(R.id.txtmsj);
        Log.i(TAG,"algo"+correo);
        recyclerView = (RecyclerView) findViewById(R.id.reciclerview1);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //crear array list de objetos tienda llamado mydataset
        msj= new ArrayList<>();
        adapter = new ChatAdapter(msj);
        recyclerView.setAdapter(adapter);
        /*String clave=FirebaseDatabase.getInstance().getReference().child("Salas").push().getKey();
        Sala sala=new Sala(correo,vendedor,key);
        FirebaseDatabase.getInstance().getReference().child("Salas").child(clave).setValue(sala);*/
        consulta= FirebaseDatabase.getInstance().getReference().child("Salas").orderByChild("objeto").equalTo(key).limitToFirst(1);
        Log.i(TAG,consulta.toString());
        consulta.addListenerForSingleValueEvent(ValueEventListener1);
        FloatingActionButton myFab = (FloatingActionButton)findViewById(R.id.mifab);
        myFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String clave=FirebaseDatabase.getInstance().getReference().child("Salas").child(salita).push().getKey();
                Log.i(TAG,"clave");
                Msm msj=new Msm(correo,(String)mens.getText());
                FirebaseDatabase.getInstance().getReference().child("Salas").child(salita).child(clave).setValue(msj);


            }


        });


    }

    ValueEventListener ValueEventListener = new ValueEventListener(){

        public void onDataChange(DataSnapshot dataSnapshot)
        {
            Log.i(TAG,"ingreso hasta aqui2");
            if(dataSnapshot.exists())
            {
                for (DataSnapshot snapshot:dataSnapshot.getChildren()) {
                    Log.i(TAG,"safmfksamolsafnmñn");
                    Log.i(TAG,"algo"+snapshot.toString());
                    System.out.print(snapshot.toString());
                    mensaje=snapshot.getValue(Msm.class);
                    Log.i(TAG,mensaje.toString());

                    msj.add(mensaje);

                    
                }

                adapter.notifyDataSetChanged();
            }


        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };

    ValueEventListener ValueEventListener1 = new ValueEventListener(){

        public void onDataChange(DataSnapshot dataSnapshot)
        {
            Log.i(TAG,"ingreso hasta aqui1");
            if(dataSnapshot.exists())
            {
                for (DataSnapshot snapshot:dataSnapshot.getChildren()) {
                    Log.i(TAG,"safmfksamolsafnmñn");
                    Log.i(TAG,"algo"+snapshot.toString());
                    System.out.print(snapshot.toString());
                    sala=snapshot.getValue(Sala.class);//se queda aqui
                    Log.i(TAG,sala.toString());
                    if(sala.getVendedor()==correo)
                    {
                    	salita=snapshot.getKey();
                    	Query cons=FirebaseDatabase.getInstance().getReference().child("Salas").child(salita).child("Msm");
                    	cons.addListenerForSingleValueEvent(ValueEventListener);
                    }
                    else
            			{
                            Log.i(TAG,"aaaaaaaaaaaasafmfksamolsafnmñn");
                                String clave=FirebaseDatabase.getInstance().getReference().child("Salas").push().getKey();
                                Sala sala=new Sala(correo,vendedor,key,mens);
                                FirebaseDatabase.getInstance().getReference().child("Salas").child(clave).setValue(sala);
            			} 
                    
                }
               
            }
            else
            {
                Log.i(TAG,"no entro a la consulta");
            }

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {

        }
    };




}
