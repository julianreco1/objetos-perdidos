package com.example.julia.lostjects;

import android.content.Context;
import android.net.Uri;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

public class PostTienda1 {
    private String imageButton;
    private String nombre;
    private String descripcion;
    private String precio;
    private String vendedor;
    private String comprador;
    private String objeto;

    public PostTienda1() {
    }

    public PostTienda1(String imageButton, String nombre, String descripcion, String precio, String vendedor, String comprador, String objeto) {
        this.imageButton = imageButton;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.precio = precio;
        this.vendedor = vendedor;
        this.comprador = comprador;
        this.objeto = objeto;
    }

    public String  getImageButton() {
        return imageButton;
    }

    public void setImageButton(String  imageButton) {

        this.imageButton = imageButton;

    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {

        this.nombre = nombre;
    }

    public String getDescripcion()
    {
        return descripcion;
    }

    public void setDescripcion(String descripcion)
    {
        this.descripcion = descripcion;
    }

    public String getPrecio()
    {
        return precio;
    }

    public void setPrecio(String precio)
    {
        this.precio = precio;
    }

    public String getVendedor()
    {
        return vendedor;
    }

    public void setVendedor(String vendedor)
    {
        this.vendedor = vendedor;
    }

    public String getComprador()
    {
        return comprador;
    }

    public void setComprador(String comprador)
    {
        this.comprador = comprador;
    }

    public String getObjeto()
    {
        return objeto;
    }

    public void setObjeto(String objeto)
    {
        this.objeto = objeto;
    }
}