package com.example.julia.lostjects;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class ChatAdapter extends RecyclerView.Adapter<ChatAdapter.ViewHolder> {
    private ArrayList<Msm> mDataset;




    public static class ViewHolder extends RecyclerView.ViewHolder  {
        // each data item is just a string in this case
        TextView mensaje;
        TextView nombre;
        Context context;

        //men mañana seguimos me voy a desconectar?
        ViewHolder(View v) {
            super(v);
            context=v.getContext();
            nombre=(TextView)v.findViewById(R.id.txtCorreo);
            mensaje=(TextView)v.findViewById(R.id.txtMensaje);



        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ChatAdapter (ArrayList<Msm> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ChatAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // create a new view
        // View v =LayoutInflater.from(parent.getContext())
        //      .inflate(R.layout.objetobton, parent, false);
        View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.plantilla1, parent, false);

        return new ChatAdapter.ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.nombre.setText( mDataset.get(position).getCorreo());
        holder.mensaje.setText( mDataset.get(position).getMensaje());
    }





    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }

}
