package com.example.julia.lostjects;

public class Msm {
    private String correo;
    private String mensaje;

    public Msm() {

    }

    public Msm(String correo, String mensaje) {
        this.correo = correo;
        this.mensaje = mensaje;
    }

    public String getCorreo() {
        return correo;
    }

    public String getMensaje() {
        return mensaje;
    }

   
    public String toString() {
        return "Msm{" +
                "correo='" + correo + '\'' +
                ", mensaje='" + mensaje + '\'' +
                '}';
    }
}
