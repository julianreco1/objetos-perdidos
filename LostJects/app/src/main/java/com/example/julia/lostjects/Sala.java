package com.example.julia.lostjects;

import java.util.ArrayList;

public class Sala {
    private String comprador;//los dos son correos
    private String vendedor;
    private String objeto;
    Msm msm;

    public Sala() {
    }

    public Sala(String comprador, String vendedor, String objeto, Msm msm) {
        this.comprador = comprador;
        this.vendedor = vendedor;
        this.objeto = objeto;
        this.msm = msm;
    }

    public String getComprador() {
        return comprador;
    }

    public void setComprador(String comprador) {
        this.comprador = comprador;
    }

    public String getVendedor() {
        return vendedor;
    }

    public void setVendedor(String vendedor) {
        this.vendedor = vendedor;
    }

    public String getObjeto() {
        return objeto;
    }

    public void setObjeto(String objeto) {
        this.objeto = objeto;
    }
}
