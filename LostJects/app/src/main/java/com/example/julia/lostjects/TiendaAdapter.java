package com.example.julia.lostjects;


import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class TiendaAdapter extends RecyclerView.Adapter<TiendaAdapter.ViewHolder> {
    private ArrayList<PostTienda1> mDataset;




    public static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        // each data item is just a string in this case
        TextView nombre;
        TextView descripcion;
        TextView precio;
        ImageView imagen;
        TextView vendedor;
        TextView comprador;
        TextView objeto;
        Context context;
        Button chat;
        TextView correo;
        //men mañana seguimos me voy a desconectar?
        ViewHolder(View v) {
            super(v);

            context=v.getContext();
            imagen=(ImageView)v.findViewById(R.id.img);
            nombre=(TextView)v.findViewById(R.id.txtNombre);
            descripcion=(TextView)v.findViewById(R.id.txtDescripcion);
            precio=(TextView)v.findViewById(R.id.txtPrecio);
            chat=(Button)v.findViewById(R.id.btnChat);
            vendedor=(TextView)v.findViewById(R.id.txtvendedor);
            objeto=(TextView)v.findViewById(R.id.txtobjeto);
            comprador=(TextView)v.findViewById(R.id.txtcomprador);
            correo=(TextView)v.findViewById(R.id.txtCorreopropio);

        }
        void setOnClickListener()
        {

            chat.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            switch (v.getId())
            {
                case R.id.btnChat:


                    Intent intent=new Intent(context,Chat.class);
                    intent.putExtra("objeto",objeto.getText());
                    intent.putExtra("propio",comprador.getText());
                    intent.putExtra("vendedor",vendedor.getText());
                    context.startActivity(intent);
                    break;
            }
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public TiendaAdapter(ArrayList<PostTienda1> myDataset) {
        mDataset = myDataset;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public TiendaAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        // create a new view
       // View v =LayoutInflater.from(parent.getContext())
         //      .inflate(R.layout.objetobton, parent, false);
        View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.objetobton, parent, false);

        return new ViewHolder(v);
    }



    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.nombre.setText( mDataset.get(position).getNombre());
        holder.descripcion.setText( mDataset.get(position).getDescripcion());
        holder.precio.setText( mDataset.get(position).getPrecio());
        holder.vendedor.setText(mDataset.get(position).getVendedor());

        holder.imagen.setImageURI(Uri.parse(mDataset.get(position).getImageButton()));
        holder.objeto.setText(mDataset.get(position).getObjeto());
        holder.comprador.setText(mDataset.get(position).getComprador());
        holder.setOnClickListener();
        //boton






    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}