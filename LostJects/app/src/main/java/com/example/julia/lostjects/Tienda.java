package com.example.julia.lostjects;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class Tienda extends AppCompatActivity {

    private RecyclerView mLostlist;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tienda);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mLostlist = (RecyclerView) findViewById(R.id.lost_list);
        mLostlist.setHasFixedSize(true);
        mLostlist.setLayoutManager(new LinearLayoutManager(this));
        mDatabase = FirebaseDatabase.getInstance().getReference().child("ObjetosTienda");
    }

    @Override
    protected void onStart() {
        super.onStart();
        FirebaseRecyclerAdapter <shop,Tienda.LostViewHolder> FBRA = new FirebaseRecyclerAdapter<shop, Tienda.LostViewHolder>(
                shop.class,
                R.layout.tienda_row,
                Tienda.LostViewHolder.class,
                mDatabase
        ) {



            protected void populateViewHolder(Tienda.LostViewHolder viewHolder, shop model, int position)  {
                viewHolder.setTitle(model.getTitle());
                viewHolder.setDesc(model.getDesc());
                viewHolder.setImage(getApplicationContext(),model.getImage());
                viewHolder.setPrecio(model.getPrecio());
            }

        };
        mLostlist.setAdapter(FBRA);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    public static class LostViewHolder extends RecyclerView.ViewHolder{

        public LostViewHolder(View itemView){
            super(itemView);
            View mView = itemView;
        }

        public void setTitle(String title){
            TextView post_title = (TextView) itemView.findViewById(R.id.textTitle);
            post_title.setText(title);
        }

        public void setDesc(String desc){
            TextView post_desc = (TextView) itemView.findViewById(R.id.textDescription);
            post_desc.setText(desc);
        }

        public void setImage(Context ctx,String image){
            ImageView post_image = (ImageView) itemView.findViewById(R.id.post_image);
            Picasso.with(ctx).load(image).into(post_image);
        }

        public void setPrecio(String precio){
            TextView post_precio = (TextView) itemView.findViewById(R.id.textPrecio);
            post_precio.setText(precio);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.addIcon){
            Intent intent = new Intent(Tienda.this,PostTienda.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}



