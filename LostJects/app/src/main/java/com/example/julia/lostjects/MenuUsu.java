package com.example.julia.lostjects;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class MenuUsu extends AppCompatActivity {
    private String correo;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_usu);
        Button tienda= findViewById(R.id.btnTienda);
        Button objetos=findViewById(R.id.btnObjetos);
        Button salir=findViewById(R.id.btnSalir);
        Button tiendavieja=findViewById(R.id.btnTiendavieja);
        Bundle objeto=getIntent().getExtras();
        String TAG="prueba";
        if(objeto != null)
        {

            correo=objeto.getString("propio");
        }
        Log.i(TAG,"añgo"+correo);

        tienda.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),TiendaNueva.class);
                intent.putExtra("propio",correo);
                startActivity(intent);
            }

        });
        tiendavieja.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),Tienda.class);
                intent.putExtra("propio",correo);
                startActivity(intent);
            }

        });
        objetos.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),MisObjetos.class);
                intent.putExtra("propio",correo);
                startActivity(intent);
            }

        });
        salir.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent=new Intent(getApplicationContext(),propietario.class);
                intent.putExtra("propio",correo);
                startActivity(intent);
            }

        });


    }

}
