package com.example.julia.sqlite;

/**
 * Created by Paulo on 27/10/2017.
 */

public class Cliente {

    private Integer Identificacion;
    private String Nombres;
    private String Apellidos;
    private String Telefono;
    private String Direccion;
    private String Correo;

    public Cliente(){

    }


    public Cliente(Integer identificacion, String nombres, String apellidos, String telefono, String direccion, String correo){
        this.Identificacion = identificacion;
        this.Nombres = nombres;
        this.Apellidos = apellidos;
        this.Telefono = telefono;
        this.Direccion = direccion;
        this.Correo = correo;

    }

    public Integer getIdentificacion() {
        return Identificacion;
    }

    public void setIdentificacion(Integer identificacion) {
        Identificacion = identificacion;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String nombres) {
        Nombres = nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String apellidos) {
        Apellidos = apellidos;
    }

    public void setCorreo(String correo) {
        Correo = correo;
    }

    public String getCorreo() {
        return Correo;
    }

    public void setDireccion(String direccion) {
        Direccion = direccion;
    }

    public String getDireccion() {
        return Direccion;
    }

    public String getTelefono() {
        return Telefono;
    }

    public void setTelefono(String telefono) {
        Telefono = telefono;
    }


}
